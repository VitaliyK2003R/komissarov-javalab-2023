package ru.kpfu.itis.komissarov.hotelservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.komissarov.hotelservice.client.cityservice.CityClient;
import ru.kpfu.itis.komissarov.hotelservice.client.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequest;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequestForUpdate;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.HotelResponse;
import ru.kpfu.itis.komissarov.hotelservice.exception.HotelNotFoundException;
import ru.kpfu.itis.komissarov.hotelservice.model.Hotel;
import ru.kpfu.itis.komissarov.hotelservice.repository.HotelRepository;
import ru.kpfu.itis.komissarov.hotelservice.util.mapper.HotelMapper;
import ru.kpfu.itis.komissarov.hotelservice.util.security.SecurityUtils;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {
    private final CityClient cityclient;
    private final HotelRepository hotelRepository;
    private final HotelMapper hotelMapper;

    @Override
    public ResponseEntity<HotelResponse> createHotel(HotelRequest hotelRequest) {
        Hotel hotel = hotelRepository.save(hotelMapper.toHotelFromHotelRequest(hotelRequest));
        UUID cityId = hotel.getCityId();
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        CityResponse cityResponse = cityclient.getCity(cityId, apiKey).getBody();
        if (cityResponse == null) {
            throw new IllegalArgumentException("City with id=" + cityId + "does not exist");
        }
        HotelResponse hotelResponse = hotelMapper.toHotelResponseFromHotel(hotel);
        return new ResponseEntity<>(hotelResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<HotelResponse> getHotel(UUID hotelId) {
        Hotel hotel = hotelRepository.findById(hotelId).orElseThrow(HotelNotFoundException::new);
        HotelResponse hotelResponse = hotelMapper.toHotelResponseFromHotel(hotel);
        return new ResponseEntity<>(hotelResponse, HttpStatus.OK);
    }

    @Override
    @Transactional
    public void updateHotel(UUID hotelId, HotelRequestForUpdate hotelRequestForUpdate) {
        Hotel hotel = hotelMapper.toHotelFromHotelRequestForUpdate(hotelRequestForUpdate);
        UUID cityId = hotel.getCityId();
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        CityResponse cityResponse = cityclient.getCity(cityId, apiKey).getBody();
        if (cityResponse == null) {
            throw new IllegalArgumentException("City with id=" + cityId + "does not exist");
        }
        Hotel updateHotel = hotelRepository.findById(hotelId).orElseThrow(HotelNotFoundException::new);
        updateHotel.setName(hotel.getName());
        updateHotel.setCityId(hotel.getCityId());
    }

    @Override
    @Transactional
    public void deleteHotel(UUID hotelId) {
        hotelRepository.deleteById(hotelId);
    }

    @Override
    public ResponseEntity<List<HotelResponse>> getAllByCityId(UUID cityId) {
        List<Hotel> hotels = hotelRepository.getAllByCityId(cityId);
        List<HotelResponse> hotelResponses = hotelMapper.toListHotelResponsesFromHotels(hotels);
        return new ResponseEntity<>(hotelResponses,HttpStatus.OK);
    }

    @Override
    @Transactional
    public void deleteCinemasByCityId(UUID cityId) {
        hotelRepository.deleteAllByCityId(cityId);
    }
}

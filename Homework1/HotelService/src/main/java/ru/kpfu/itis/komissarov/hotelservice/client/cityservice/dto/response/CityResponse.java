package ru.kpfu.itis.komissarov.hotelservice.client.cityservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CityResponse {
    private UUID id;
    private String name;
}

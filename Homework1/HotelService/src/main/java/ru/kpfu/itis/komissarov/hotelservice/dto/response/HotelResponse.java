package ru.kpfu.itis.komissarov.hotelservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class HotelResponse {
    private UUID id;
    private String name;
    private List<RoomResponse> roomResponses;
    private UUID cityId;
}

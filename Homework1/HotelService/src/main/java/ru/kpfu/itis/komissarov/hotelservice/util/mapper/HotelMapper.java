package ru.kpfu.itis.komissarov.hotelservice.util.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequest;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequestForUpdate;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.HotelResponse;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.RoomResponse;
import ru.kpfu.itis.komissarov.hotelservice.model.Hotel;
import ru.kpfu.itis.komissarov.hotelservice.model.Room;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class HotelMapper {
    @Autowired
    private RoomMapper roomMapper;

    @Mapping(source = "name", target = "name")
    @Mapping(source = "countRooms", target = "rooms", qualifiedByName = "initRoomsSet")
    @Mapping(source = "cityId", target = "cityId")
    public abstract Hotel toHotelFromHotelRequest(HotelRequest hotelRequest);

    @Named(value = "initRoomsSet")
    public Set<Room> initRoomsSet(Integer countRooms) {
        return roomMapper.initRoomsSet(countRooms);
    }


    @Mapping(source = "name", target = "name")
    @Mapping(source = "cityId", target = "cityId")
    public abstract Hotel toHotelFromHotelRequestForUpdate(HotelRequestForUpdate hotelRequestForUpdate);


    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "rooms", target = "roomResponses", qualifiedByName = "toRoomResponseFromRoom")
    @Mapping(source = "cityId", target = "cityId")
    public abstract HotelResponse toHotelResponseFromHotel(Hotel hotel);

    @Named(value = "toRoomResponseFromRoom")
    public RoomResponse toRoomResponseFromRoom(Room room) {
        return roomMapper.toRoomResponseFromRoom(room);
    }


    public abstract List<HotelResponse> toListHotelResponsesFromHotels(List<Hotel> hotels);
}
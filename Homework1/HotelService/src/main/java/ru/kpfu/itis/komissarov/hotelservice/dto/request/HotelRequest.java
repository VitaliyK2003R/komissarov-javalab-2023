package ru.kpfu.itis.komissarov.hotelservice.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class HotelRequest {
    @NotBlank
    private String name;
    @NotNull
    private Integer countRooms;
    @NotNull
    private UUID cityId;
}

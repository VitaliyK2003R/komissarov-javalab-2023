package ru.kpfu.itis.komissarov.hotelservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.komissarov.hotelservice.model.Hotel;

import java.util.List;
import java.util.UUID;

public interface HotelRepository extends JpaRepository<Hotel, UUID> {
    List<Hotel> getAllByCityId(UUID cityId);
    void deleteAllByCityId(UUID cityId);
}

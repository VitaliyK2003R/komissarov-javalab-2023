package ru.kpfu.itis.komissarov.hotelservice.util.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.RoomResponse;
import ru.kpfu.itis.komissarov.hotelservice.model.Room;

import java.util.HashSet;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class RoomMapper {
    public Set<Room> initRoomsSet(Integer countRooms) {
        Set<Room> rooms = new HashSet<>(countRooms);
        for (int i = 1; i <= countRooms; i++) {
            rooms.add(Room.builder().number(i).build());
        }
        return rooms;
    }


    @Mapping(source = "number", target = "number")
    public abstract RoomResponse toRoomResponseFromRoom(Room room);
}

package ru.kpfu.itis.komissarov.hotelservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.komissarov.hotelservice.api.HotelApi;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequest;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequestForUpdate;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.HotelResponse;
import ru.kpfu.itis.komissarov.hotelservice.service.HotelService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class HotelController implements HotelApi {
    private final HotelService hotelService;

    @Override
    public ResponseEntity<HotelResponse> createHotel(HotelRequest hotelRequest) {
        return hotelService.createHotel(hotelRequest);
    }

    @Override
    public ResponseEntity<HotelResponse> getHotel(UUID hotelId) {
        return hotelService.getHotel(hotelId);
    }

    @Override
    public void updateHotel(UUID hotelId, HotelRequestForUpdate hotelRequestForUpdate) {
        hotelService.updateHotel(hotelId, hotelRequestForUpdate);
    }

    @Override
    public void deleteHotel(UUID hotelId) {
        hotelService.deleteHotel(hotelId);
    }

    @Override
    public ResponseEntity<List<HotelResponse>> getAllByCityId(UUID cityId) {
        return hotelService.getAllByCityId(cityId);
    }

    @Override
    public void deleteCinemasByCityId(UUID cityId) {
        hotelService.deleteCinemasByCityId(cityId);
    }
}

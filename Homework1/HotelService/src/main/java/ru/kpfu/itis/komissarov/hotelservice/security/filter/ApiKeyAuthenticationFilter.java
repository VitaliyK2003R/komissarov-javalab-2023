package ru.kpfu.itis.komissarov.hotelservice.security.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.kpfu.itis.komissarov.hotelservice.security.authentication.ApiKeyAuthenticationToken;

import java.io.IOException;

@RequiredArgsConstructor
@Component
public class ApiKeyAuthenticationFilter extends OncePerRequestFilter {
    public static final String API_KEY_PARAMETER = "api-key";
    private final AuthenticationManager authenticationManager;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String requestApiKey = request.getParameter(API_KEY_PARAMETER);

        if (requestApiKey != null) {
            SecurityContextHolder.getContext().setAuthentication(authenticationManager
                    .authenticate(new ApiKeyAuthenticationToken(requestApiKey,AuthorityUtils.NO_AUTHORITIES)));
        }

        filterChain.doFilter(request,response);
    }
}

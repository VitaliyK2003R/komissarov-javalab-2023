package ru.kpfu.itis.komissarov.hotelservice.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequest;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequestForUpdate;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.HotelResponse;
import ru.kpfu.itis.komissarov.hotelservice.model.Hotel;

import java.util.List;
import java.util.UUID;

public interface HotelService {
    ResponseEntity<HotelResponse> createHotel(HotelRequest hotelRequest);

    ResponseEntity<HotelResponse> getHotel(UUID hotelId);

    void updateHotel(UUID hotelId, HotelRequestForUpdate hotelRequestForUpdate);

    void deleteHotel(@PathVariable UUID hotelId);

    ResponseEntity<List<HotelResponse>> getAllByCityId(@RequestParam UUID cityId);

    @DeleteMapping
    void deleteCinemasByCityId(@RequestParam UUID cityId);
}

package ru.kpfu.itis.komissarov.hotelservice.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequest;
import ru.kpfu.itis.komissarov.hotelservice.dto.request.HotelRequestForUpdate;
import ru.kpfu.itis.komissarov.hotelservice.dto.response.HotelResponse;

import java.util.List;
import java.util.UUID;

@RequestMapping("/api/v1/hotels")
public interface HotelApi {
    @PostMapping
    ResponseEntity<HotelResponse> createHotel(@RequestBody @Valid HotelRequest hotelRequest);

    @GetMapping("/{hotelId}")
    ResponseEntity<HotelResponse> getHotel(@PathVariable UUID hotelId);

    @PutMapping("/{hotelId}")
    void updateHotel(@PathVariable UUID hotelId, @RequestBody @Valid HotelRequestForUpdate hotelRequestForUpdate);

    @DeleteMapping("/{hotelId}")
    void deleteHotel(@PathVariable UUID hotelId);

    @GetMapping
    ResponseEntity<List<HotelResponse>> getAllByCityId(@RequestParam UUID cityId);

    @DeleteMapping
    void deleteCinemasByCityId(@RequestParam UUID cityId);
}

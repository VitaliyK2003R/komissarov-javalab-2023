package ru.kpfu.itis.komissarov.cityservice.util.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.kpfu.itis.komissarov.cityservice.dto.request.CityRequest;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityWithCinemasAndHotelsResponse;
import ru.kpfu.itis.komissarov.cityservice.model.City;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CityMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(source = "name", target = "name")
    City toCityFromCityRequest(CityRequest cityRequest);


    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    CityResponse toCityResponseFromCity(City city);


    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    CityWithCinemasAndHotelsResponse toCityWithCinemasAndHotelsResponseFromCity(City city);


    List<CityResponse> toListCityResponsesFromListCities(List<City> cities);


    List<CityWithCinemasAndHotelsResponse> toListCityWithCinemasAndHotelsResponsesFromListCities(List<City> cities);
}

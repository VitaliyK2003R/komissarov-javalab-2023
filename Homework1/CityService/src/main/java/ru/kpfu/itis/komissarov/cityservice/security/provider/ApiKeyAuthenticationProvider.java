package ru.kpfu.itis.komissarov.cityservice.security.provider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.komissarov.cityservice.security.authentication.ApiKeyAuthenticationToken;

import java.util.Collections;

@Component
public class ApiKeyAuthenticationProvider implements AuthenticationProvider {
    @Value(value = "${api.key}")
    private String API_KEY;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (!API_KEY.equals(authentication.getPrincipal())) {
            throw new BadCredentialsException("Invalid credentials");
        }
        return new UsernamePasswordAuthenticationToken(API_KEY, null,
                                                Collections.singleton(new SimpleGrantedAuthority("CITY_SERVICE")));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return ApiKeyAuthenticationToken.class.equals(authentication);
    }
}

package ru.kpfu.itis.komissarov.cityservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.komissarov.cityservice.client.cinemaservice.CinemaClient;
import ru.kpfu.itis.komissarov.cityservice.client.cinemaservice.dto.response.CinemaResponse;
import ru.kpfu.itis.komissarov.cityservice.client.hotelservice.HotelClient;
import ru.kpfu.itis.komissarov.cityservice.client.hotelservice.dto.response.HotelResponse;
import ru.kpfu.itis.komissarov.cityservice.dto.request.CityRequest;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityWithCinemasAndHotelsResponse;
import ru.kpfu.itis.komissarov.cityservice.exception.CityNotFoundException;
import ru.kpfu.itis.komissarov.cityservice.model.City;
import ru.kpfu.itis.komissarov.cityservice.repository.CityRepository;
import ru.kpfu.itis.komissarov.cityservice.util.mapper.CityMapper;
import ru.kpfu.itis.komissarov.cityservice.util.security.SecurityUtils;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CityServiceImpl implements CityService {
    private final CinemaClient cinemaClient;
    private final HotelClient hotelClient;
    private final CityRepository cityRepository;
    private final CityMapper cityMapper;

    @Override
    public ResponseEntity<CityResponse> createCity(CityRequest cityRequest) {
        CityResponse cityResponse = cityMapper.toCityResponseFromCity(
                                                cityRepository.save(
                                                                cityMapper.toCityFromCityRequest(cityRequest)));
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CityWithCinemasAndHotelsResponse> getCityWithCinemasAndHotels(UUID cityId) {
        City city = cityRepository.findById(cityId).orElseThrow(CityNotFoundException::new);
        CityWithCinemasAndHotelsResponse cityWithCinemasAndHotelsResponse =
                                                        cityMapper.toCityWithCinemasAndHotelsResponseFromCity(city);
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        List<CinemaResponse> cityResponses = cinemaClient.getAllByCityId(cityId, apiKey).getBody();
        cityWithCinemasAndHotelsResponse.setCinemaResponses(cityResponses);
        List<HotelResponse> hotelResponses = hotelClient.getAllByCityId(cityId, apiKey).getBody();
        cityWithCinemasAndHotelsResponse.setHotelResponses(hotelResponses);
        return new ResponseEntity<>(cityWithCinemasAndHotelsResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CityResponse> getCity(UUID cityId) {
        City city = cityRepository.findById(cityId).orElseThrow(CityNotFoundException::new);
        CityResponse cityResponse = cityMapper.toCityResponseFromCity(city);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CityWithCinemasAndHotelsResponse>> getAll() {
        List<City> cities = cityRepository.findAll();
        List<CityWithCinemasAndHotelsResponse> cityWithCinemasAndHotelsResponses =
                                            cityMapper.toListCityWithCinemasAndHotelsResponsesFromListCities(cities);
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        for (CityWithCinemasAndHotelsResponse cityResponses: cityWithCinemasAndHotelsResponses) {
            List<CinemaResponse> cinemaResponses = cinemaClient.getAllByCityId(cityResponses.getId(), apiKey).getBody();
            cityResponses.setCinemaResponses(cinemaResponses);
        }
        return new ResponseEntity<>(cityWithCinemasAndHotelsResponses, HttpStatus.OK);
    }

    @Override
    public void update(UUID cityId, CityRequest cityRequest) {
        City city = cityMapper.toCityFromCityRequest(cityRequest);
        City updateCity = cityRepository.findById(cityId).orElseThrow(CityNotFoundException::new);
        updateCity.setName(city.getName());
        cityRepository.saveAndFlush(updateCity);
    }

    @Override
    @Transactional
    public void deleteCity(UUID cityId) {
        cityRepository.deleteById(cityId);
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        cinemaClient.deleteCinemaByCityId(cityId, apiKey);
        hotelClient.deleteCinemasByCityId(cityId, apiKey);
    }
}

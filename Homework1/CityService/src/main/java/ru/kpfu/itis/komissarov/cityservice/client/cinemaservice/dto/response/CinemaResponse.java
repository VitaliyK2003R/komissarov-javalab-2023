package ru.kpfu.itis.komissarov.cityservice.client.cinemaservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CinemaResponse {
    private UUID id;
    private String name;
}

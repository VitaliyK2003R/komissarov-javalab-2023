package ru.kpfu.itis.komissarov.cityservice.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.komissarov.cityservice.dto.request.CityRequest;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityWithCinemasAndHotelsResponse;

import java.util.List;
import java.util.UUID;

@RequestMapping("/api/v1/cities")
public interface CityApi {
    @PostMapping
    ResponseEntity<CityResponse> createCity(@RequestBody @Valid CityRequest cityRequest);

    @GetMapping("/{cityId}/cinemas-hotels/info")
    ResponseEntity<CityWithCinemasAndHotelsResponse> getCityWithCinemasAndHotels(@PathVariable UUID cityId);

    @GetMapping("/{cityId}")
    ResponseEntity<CityResponse> getCity(@PathVariable UUID cityId);

    @GetMapping
    ResponseEntity<List<CityWithCinemasAndHotelsResponse>> getAll();

    @PutMapping("/{cityId}")
    void update(@PathVariable UUID cityId, @RequestBody @Valid CityRequest cityRequest);

    @DeleteMapping("/{cityId}")
    void deleteCity(@PathVariable UUID cityId);


}

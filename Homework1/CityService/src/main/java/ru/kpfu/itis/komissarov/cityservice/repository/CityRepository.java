package ru.kpfu.itis.komissarov.cityservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.komissarov.cityservice.model.City;

import java.util.UUID;

public interface CityRepository extends JpaRepository<City, UUID> {
}

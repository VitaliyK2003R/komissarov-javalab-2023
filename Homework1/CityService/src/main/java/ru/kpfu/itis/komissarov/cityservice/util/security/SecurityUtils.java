package ru.kpfu.itis.komissarov.cityservice.util.security;

import org.springframework.security.core.context.SecurityContextHolder;

public final class SecurityUtils {
    public static Object getCurrentAuthenticationPrinciple() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}

package ru.kpfu.itis.komissarov.cityservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.komissarov.cityservice.client.cinemaservice.dto.response.CinemaResponse;
import ru.kpfu.itis.komissarov.cityservice.client.hotelservice.dto.response.HotelResponse;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CityWithCinemasAndHotelsResponse {
    private UUID id;
    private String name;
    private List<CinemaResponse> cinemaResponses;
    private List<HotelResponse> hotelResponses;
}

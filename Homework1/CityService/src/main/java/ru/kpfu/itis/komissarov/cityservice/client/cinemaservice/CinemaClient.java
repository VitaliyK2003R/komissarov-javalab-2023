package ru.kpfu.itis.komissarov.cityservice.client.cinemaservice;

import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.komissarov.cityservice.client.cinemaservice.dto.request.CinemaRequest;
import ru.kpfu.itis.komissarov.cityservice.client.cinemaservice.dto.response.CinemaResponse;

import java.util.List;
import java.util.UUID;

@FeignClient(name = "cinemaClient", url = "${client.service.cinema-uri}")
public interface CinemaClient {
    @GetMapping
    ResponseEntity<List<CinemaResponse>> getAllByCityId(@RequestParam UUID cityId,
                                                        @RequestParam(name = "api-key") String apiKey);

    @DeleteMapping
    void deleteCinemaByCityId(@RequestParam UUID cityId, @RequestParam(name = "api-key") String apiKey);
}

package ru.kpfu.itis.komissarov.cityservice.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import ru.kpfu.itis.komissarov.cityservice.dto.request.CityRequest;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityWithCinemasAndHotelsResponse;

import java.util.List;
import java.util.UUID;

public interface CityService {
    ResponseEntity<CityResponse> createCity(CityRequest cityRequest);

    ResponseEntity<CityWithCinemasAndHotelsResponse> getCityWithCinemasAndHotels(UUID cityId);

    ResponseEntity<CityResponse> getCity(UUID cityId);

    ResponseEntity<List<CityWithCinemasAndHotelsResponse>> getAll();

    void update(UUID cityId, CityRequest cityRequest);

    void deleteCity(UUID cityId);
}

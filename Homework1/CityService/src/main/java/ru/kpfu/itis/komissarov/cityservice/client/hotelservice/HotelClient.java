package ru.kpfu.itis.komissarov.cityservice.client.hotelservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.komissarov.cityservice.client.hotelservice.dto.response.HotelResponse;

import java.util.List;
import java.util.UUID;

@FeignClient(name = "hotelService", url = "${client.service.hotel-uri}")
public interface HotelClient {

    @GetMapping
    ResponseEntity<List<HotelResponse>> getAllByCityId(@RequestParam UUID cityId,
                                                       @RequestParam(name = "api-key") String apiKey);

    @DeleteMapping
    void deleteCinemasByCityId(@RequestParam UUID cityId, @RequestParam(name = "api-key") String apiKey);
}

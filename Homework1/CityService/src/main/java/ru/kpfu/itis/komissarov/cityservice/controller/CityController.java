package ru.kpfu.itis.komissarov.cityservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.komissarov.cityservice.api.CityApi;
import ru.kpfu.itis.komissarov.cityservice.dto.request.CityRequest;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.cityservice.dto.response.CityWithCinemasAndHotelsResponse;
import ru.kpfu.itis.komissarov.cityservice.service.CityService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CityController implements CityApi {
    private final CityService cityService;

    @Override
    public ResponseEntity<CityResponse> createCity(CityRequest cityRequest) {
        return cityService.createCity(cityRequest);
    }

    @Override
    public ResponseEntity<CityWithCinemasAndHotelsResponse> getCityWithCinemasAndHotels(UUID cityId) {
        return cityService.getCityWithCinemasAndHotels(cityId);
    }

    @Override
    public ResponseEntity<CityResponse> getCity(UUID cityId) {
        return cityService.getCity(cityId);
    }

    @Override
    public ResponseEntity<List<CityWithCinemasAndHotelsResponse>> getAll() {
        return cityService.getAll();
    }

    @Override
    public void update(UUID cityId, CityRequest cityRequest) {
        cityService.update(cityId,cityRequest);
    }

    @Override
    public void deleteCity(UUID cityId) {
        cityService.deleteCity(cityId);
    }
}

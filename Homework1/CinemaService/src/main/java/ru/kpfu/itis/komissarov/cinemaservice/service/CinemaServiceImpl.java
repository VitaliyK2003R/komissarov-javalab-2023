package ru.kpfu.itis.komissarov.cinemaservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.komissarov.cinemaservice.client.cityservice.CityClient;
import ru.kpfu.itis.komissarov.cinemaservice.client.cityservice.dto.response.CityResponse;
import ru.kpfu.itis.komissarov.cinemaservice.dto.request.CinemaRequest;
import ru.kpfu.itis.komissarov.cinemaservice.dto.response.CinemaResponse;
import ru.kpfu.itis.komissarov.cinemaservice.model.Cinema;
import ru.kpfu.itis.komissarov.cinemaservice.util.mapper.CinemaMapper;
import ru.kpfu.itis.komissarov.cinemaservice.exception.CinemaNotFoundException;
import ru.kpfu.itis.komissarov.cinemaservice.repository.CinemaRepository;
import ru.kpfu.itis.komissarov.cinemaservice.util.security.SecurityUtils;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CinemaServiceImpl implements CinemaService {
    private final CityClient cityClient;
    private final CinemaRepository cinemaRepository;
    private final CinemaMapper cinemaMapper;

    @Override
    public ResponseEntity<CinemaResponse> createCinema(CinemaRequest cinemaRequest) {
        Cinema cinema = cinemaRepository.save(cinemaMapper.toCinemaFromCinemaRequest(cinemaRequest));
        UUID cityId = cinema.getCityId();
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        CityResponse cityResponse = cityClient.getCity(cityId, apiKey).getBody();
        if (cityResponse == null) {
            throw new IllegalArgumentException("City with id=" + cityId + "does not exist");
        }
        CinemaResponse cinemaResponse = cinemaMapper.toCinemaResponseFromCinema(cinema);
        return new ResponseEntity<>(cinemaResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CinemaResponse> getCinema(UUID cinemaId) {
        Cinema cinema = cinemaRepository.findById(cinemaId).orElseThrow(CinemaNotFoundException::new);
        CinemaResponse cinemaResponse = cinemaMapper.toCinemaResponseFromCinema(cinema);
        return new ResponseEntity<>(cinemaResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CinemaResponse>> getAll() {
        List<Cinema> cinemas = cinemaRepository.findAll();
        List<CinemaResponse> cinemaResponses = cinemaMapper.toListCinemaResponsesFromListCinemas(cinemas);
        return new ResponseEntity<>(cinemaResponses, HttpStatus.OK);
    }

    @Override
    public void updateCinema(UUID cinemaId, CinemaRequest cinemaRequest) {
        Cinema cinema = cinemaMapper.toCinemaFromCinemaRequest(cinemaRequest);
        UUID cityId = cinema.getCityId();
        String apiKey = (String) SecurityUtils.getCurrentAuthenticationPrinciple();
        CityResponse cityResponse = cityClient.getCity(cityId, apiKey).getBody();
        if (cityResponse == null) {
            throw new IllegalArgumentException("City with id=" + cityId + "does not exist");
        }
        Cinema updateCinema = cinemaRepository.findById(cinemaId).orElseThrow(CinemaNotFoundException::new);
        updateCinema.setName(cinema.getName());
        updateCinema.setCityId(cinema.getCityId());
        cinemaRepository.saveAndFlush(updateCinema);
    }

    @Override
    public void deleteCinema(UUID cinemaId) {
        cinemaRepository.deleteById(cinemaId);
        cinemaRepository.flush();
    }

    @Override
    public ResponseEntity<List<CinemaResponse>> getAllByCityId(UUID cityId) {
        List<Cinema> cinemas = cinemaRepository.findAllByCityId(cityId);
        List<CinemaResponse> cinemaResponses = cinemaMapper.toListCinemaResponsesFromListCinemas(cinemas);
        return new ResponseEntity<>(cinemaResponses, HttpStatus.OK);
    }

    @Override
    @Transactional
    public void deleteCinemasByCityId(UUID cityId) {
        cinemaRepository.deleteAllByCityId(cityId);
    }
}

package ru.kpfu.itis.komissarov.cinemaservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.komissarov.cinemaservice.model.Cinema;

import java.util.List;
import java.util.UUID;

public interface CinemaRepository extends JpaRepository<Cinema, UUID> {
    List<Cinema> findAllByCityId(UUID cityId);

    void deleteAllByCityId(UUID cityId);
}

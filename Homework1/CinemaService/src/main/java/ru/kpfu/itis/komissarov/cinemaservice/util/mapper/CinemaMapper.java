package ru.kpfu.itis.komissarov.cinemaservice.util.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.kpfu.itis.komissarov.cinemaservice.dto.request.CinemaRequest;
import ru.kpfu.itis.komissarov.cinemaservice.dto.response.CinemaResponse;
import ru.kpfu.itis.komissarov.cinemaservice.model.Cinema;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CinemaMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(source = "name", target = "name")
    @Mapping(source = "cityId", target = "cityId")
    Cinema toCinemaFromCinemaRequest(CinemaRequest cinemaRequest);


    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    CinemaResponse toCinemaResponseFromCinema(Cinema cinema);


    List<CinemaResponse> toListCinemaResponsesFromListCinemas(List<Cinema> cinemas);
}

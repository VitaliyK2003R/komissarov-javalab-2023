package ru.kpfu.itis.komissarov.cinemaservice.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CinemaRequest {
    @NotBlank
    private String name;
    @NotNull
    private UUID cityId;
}

package ru.kpfu.itis.komissarov.cinemaservice.client.cityservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.komissarov.cinemaservice.client.cityservice.dto.response.CityResponse;

import java.util.UUID;

@FeignClient(name = "cityService", url = "${client.service.city-uri}")
public interface CityClient {
    @GetMapping("/{cityId}")
    ResponseEntity<CityResponse> getCity(@PathVariable UUID cityId, @RequestParam(name = "api-key") String apiKey);
}

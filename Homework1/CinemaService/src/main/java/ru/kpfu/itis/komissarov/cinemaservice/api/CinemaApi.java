package ru.kpfu.itis.komissarov.cinemaservice.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.komissarov.cinemaservice.dto.request.CinemaRequest;
import ru.kpfu.itis.komissarov.cinemaservice.dto.response.CinemaResponse;

import java.util.List;
import java.util.UUID;

@RequestMapping("/api/v1/cinemas")
public interface CinemaApi {
    @PostMapping
    ResponseEntity<CinemaResponse> createCinema(@RequestBody @Valid CinemaRequest cinemaRequest);

    @GetMapping("/{cinemaId}")
    ResponseEntity<CinemaResponse> getCinema(@PathVariable UUID cinemaId);

    @PutMapping("/{cinemaId}")
    void updateCinema(@PathVariable UUID cinemaId, @RequestBody @Valid CinemaRequest cinemaRequest);

    @DeleteMapping("/{cinemaId}")
    void deleteCinema(@PathVariable UUID cinemaId);

    @GetMapping
    ResponseEntity<List<CinemaResponse>> getAllByCityId(@RequestParam UUID cityId);

    @DeleteMapping
    void deleteCinemasByCityId(@RequestParam UUID cityId);
}

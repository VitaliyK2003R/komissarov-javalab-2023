package ru.kpfu.itis.komissarov.cinemaservice.service;

import org.springframework.http.ResponseEntity;
import ru.kpfu.itis.komissarov.cinemaservice.dto.request.CinemaRequest;
import ru.kpfu.itis.komissarov.cinemaservice.dto.response.CinemaResponse;

import java.util.List;
import java.util.UUID;

public interface CinemaService {
    ResponseEntity<CinemaResponse> createCinema(CinemaRequest cinemaRequest);

    ResponseEntity<CinemaResponse> getCinema(UUID cinemaId);

    ResponseEntity<List<CinemaResponse>> getAll();

    void updateCinema(UUID cinemaId, CinemaRequest cinemaRequest);

    void deleteCinema(UUID cinemaId);

    ResponseEntity<List<CinemaResponse>> getAllByCityId(UUID cityId);

    void deleteCinemasByCityId(UUID cityId);
}

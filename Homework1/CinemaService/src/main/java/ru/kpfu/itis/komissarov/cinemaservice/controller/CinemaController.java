package ru.kpfu.itis.komissarov.cinemaservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.komissarov.cinemaservice.api.CinemaApi;
import ru.kpfu.itis.komissarov.cinemaservice.dto.request.CinemaRequest;
import ru.kpfu.itis.komissarov.cinemaservice.dto.response.CinemaResponse;
import ru.kpfu.itis.komissarov.cinemaservice.service.CinemaService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CinemaController implements CinemaApi {
    private final CinemaService cinemaService;

    @Override
    public ResponseEntity<CinemaResponse> createCinema(CinemaRequest cinemaRequest) {
        return cinemaService.createCinema(cinemaRequest);
    }

    @Override
    public ResponseEntity<CinemaResponse> getCinema(UUID cinemaId) {
        return cinemaService.getCinema(cinemaId);
    }

    @Override
    public void updateCinema(UUID cinemaId, CinemaRequest cinemaRequest) {
        cinemaService.updateCinema(cinemaId, cinemaRequest);
    }

    @Override
    public void deleteCinema(UUID cinemaId) {
        cinemaService.deleteCinema(cinemaId);
    }

    @Override
    public ResponseEntity<List<CinemaResponse>> getAllByCityId(UUID cityId) {
        return cinemaService.getAllByCityId(cityId);
    }

    @Override
    public void deleteCinemasByCityId(UUID cityId) {
        cinemaService.deleteCinemasByCityId(cityId);
    }
}
